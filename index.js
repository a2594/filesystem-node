//Módulo de filesystem o fs
const fs = require("fs/promises"); //import fs from "fs"
const path = require("path");

const requirementsPath = path.resolve("requerimientos.txt");

// ||| CALLBACKS ||| Non blocking I/O
//Método para leer un archivo
fs.readFile(requirementsPath, "utf8", (error, data) => {
    if(!error){ //Si no hay errores al momento de querer leer el archivo
        return console.log(data);
    }
    //Regresaremos un error
    return console.log(error);
});

console.log("Continuando con la ejecución...");

fs.writeFile(requirementsPath, "hello world!", (error) => {
    if(error){
        console.log("No se ha podido escribir en este archivo");
    }
});

// ||| PROMESAS Y ASYNC/AWAIT ||| 
fs.readFile(requirementsPath, "utf8")
    .then(data => console.log(data))
    .catch(error => console.log(error));

(async () => {
    try{
        const data = await fs.readFile(requirementsPath, "utf8");
        console.log(data);
    }catch(error){
        console.log(error)
    }
})();

